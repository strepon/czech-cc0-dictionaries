#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from SPARQLWrapper import SPARQLWrapper, JSON
import pywikibot
import json
import pandas
import asyncio
import time

output_file_name = "wikidata_cs_lexemes_raw.txt"

lexical_categories = {"Q1084": "noun", "Q34698": "adjective", "Q24905": "verb",
    "Q147276": "proper noun", "Q134830": "prefix", "Q102047": "suffix", "Q104051989": "suffix",
    "Q187931": "phrase", "Q5551966": "phraseme", "Q3976700": "adjectival suffix",
    "Q130270424": "interrogative expression", "Q1624034": "adage",  "Q35102": "proverb" }
noun_genders = {"Q54020116": "m", "Q52943434": "i", "Q1775415": "f", "Q1775461": "n"}
verb_aspects = {"Q371427": "i", "Q1424306": "p"}

def query_czech_lexeme_ids():
    sparql = SPARQLWrapper("https://query.wikidata.org/sparql")
    sparql.setQuery("""SELECT ?l ?lemma  WHERE {
       ?l a ontolex:LexicalEntry ; dct:language wd:Q9056 ;
            wikibase:lemma ?lemma .
    }""")
    sparql.setReturnFormat(JSON)
    results = sparql.query().convert()
    lexeme_ids = []
    for result in results["results"]["bindings"]:
        lexeme_ids.append(result["l"]["value"].split("/")[-1])
    return lexeme_ids

async def load_lexeme(wikidata_repo, lexeme_id, attempt = 0):
    lexeme_page = pywikibot.Page(wikidata_repo, lexeme_id, ns=146) # 146 means "Lexeme" namespace
    json.loads(lexeme_page.get())

async def load_lexemes(wikidata_repo, lexeme_ids):
    lexeme_requests = []
    for lexeme_number, lexeme_id in enumerate(lexeme_ids):
        lexeme_requests.append(load_lexeme(wikidata_repo, lexeme_id))
    return await asyncio.gather(*lexeme_requests)

def get_language_styles(claims):
    styles = []
    if "P6191" in claims:
        for prop in claims["P6191"]:
            styles.append(prop["mainsnak"]["datavalue"]["value"]["id"])
    return ",".join(styles)

def get_grammatical_property(claims, property_id, all_values):
    if property_id in lexeme["claims"]:
        lexeme_properties = claims[property_id]
        for prop in lexeme_properties:
            value_id = prop["mainsnak"]["datavalue"]["value"]["id"]
            if value_id in all_values:
                return all_values[value_id]
    return ""

def get_gender_or_aspect(lexeme, lexical_category):
    if lexical_category in ["noun", "proper noun"]:
        return get_grammatical_property(lexeme["claims"], "P5185", noun_genders) # grammatical gender
    elif lexical_category == "verb":
        return get_grammatical_property(lexeme["claims"], "P7486", verb_aspects) # grammatical aspect
    else:
        return ""

def get_lexical_category(lexeme):
    if lexeme["lexicalCategory"] in lexical_categories:
        return lexical_categories[lexeme["lexicalCategory"]]
    else:
        return ""

def get_lemma_value(lexeme, language):
    return lexeme["lemmas"][language]["value"]

lexeme_ids = query_czech_lexeme_ids()
print(f'Found {len(lexeme_ids)} lexemes.')

wikidata_repo = pywikibot.Site("wikidata", "wikidata").data_repository()

batch_size = 10
output_created = False
start_time = time.time()
for lexeme_index in range(0, len(lexeme_ids), batch_size):
    print(f'Processing lexeme: {lexeme_index} of {len(lexeme_ids)}')
    lexemes = asyncio.run(load_lexemes(wikidata_repo, lexeme_ids[lexeme_index:(lexeme_index + batch_size)]))

    data_frame = pandas.DataFrame()
    for lexeme in lexemes:
        if lexeme is None:
            continue
        language_variants = len(lexeme["lemmas"].keys())

        # determine lexeme category, if relevant
        lexeme_category = get_lexical_category(lexeme)

        # lemmatise
        lexeme_lemmas = [] # for output
        for language in lexeme["lemmas"]:
            lexeme_lemmas.append(get_lemma_value(lexeme, language))

        # determine gender/aspect, if relevant
        # use category only if one of the conditions is met
        # - lemma consists of a single word (can be multiple variants of the same word)
        # - is a reflexive verb with "si"/"se"
        use_category = (len(lexeme_lemmas) == language_variants or
            (len(lexeme_lemmas) == 2 and (lexeme_lemmas[1] in ["se", "si"]) and lexeme_category == "verb"))
        if use_category:
            lexeme_gender_or_aspect = get_gender_or_aspect(lexeme, lexeme_category) # for output
        else:
            lexeme_category = ""
            lexeme_gender_or_aspect = ""

        # determine lexeme styles
        lexeme_styles = [] # for output
        for sense in lexeme["senses"]:
            lexeme_styles.append(get_language_styles(sense["claims"]))

        # determine lexeme forms and form styles
        lexeme_forms = [""] * len(lexeme_lemmas) # for output
        lexeme_form_styles = [] # for output
        for form in lexeme["forms"]:
            i = 0 # iterative over form representation splits
            for language in form["representations"]:
                if len(lexeme_forms[i]) > 0:
                    lexeme_forms[i] += ";"
                lexeme_forms[i] += form["representations"][language]["value"]
                i += 1
            lexeme_form_styles.append(get_language_styles(form["claims"]))

        output_data = {}
        output_data["lemma"] = lexeme_lemmas
        output_data["po"] = len(lexeme_lemmas) * [lexeme_category]
        output_data["gender_aspect"] = len(lexeme_lemmas) * [lexeme_gender_or_aspect]
        output_data["style"] = len(lexeme_lemmas) * [";".join(lexeme_styles)]
        output_data["forms"] = lexeme_forms
        output_data["form_styles"] = len(lexeme_lemmas) * [";".join(lexeme_form_styles)]
        # data_frame = pandas.DataFrame(output_data)
        data_frame = pandas.concat([data_frame, pandas.DataFrame(output_data)])
    if not output_created:
        # first line - write with header
        data_frame.to_csv(output_file_name, sep=":", index=False, header=True, mode='w')
        output_created = True
    else:
        # next lines - append
        data_frame.to_csv(output_file_name, sep=":", index=False, header=False, mode='a')

print(f'Processed {len(lexeme_ids)} lexemes, output written to {output_file_name}.')
print(time.time() - start_time)
