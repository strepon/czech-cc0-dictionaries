# "unmunching" script for Hunspell dictionaries, based on Spylls (full Python port of Hunspell)
# from https://github.com/zverok/spylls/blob/master/examples/unmunch.py
# with a fix for Czech dictionaries as submitted on: https://github.com/zverok/spylls/pull/23/files
def unmunch(word, aff):
    result = set()

    if aff.FORBIDDENWORD and aff.FORBIDDENWORD in word.flags:
        return result

    if not (aff.NEEDAFFIX and aff.NEEDAFFIX in word.flags):
        result.add(word.stem)

    suffixes = [
        suffix
        for flag in word.flags
        for suffix in aff.SFX.get(flag, [])
        if suffix.cond_regexp.search(word.stem)
    ]
    prefixes = [
        prefix
        for flag in word.flags
        for prefix in aff.PFX.get(flag, [])
        if prefix.cond_regexp.search(word.stem)
    ]

    for suffix in suffixes:
        root = word.stem[0:-len(suffix.strip)] if suffix.strip else word.stem
        suffixed = root + suffix.add
        if not (aff.NEEDAFFIX and aff.NEEDAFFIX in suffix.flags):
            result.add(suffixed)

        if suffix.crossproduct:
            additional_prefixes = [
                prefix
                for flag in suffix.flags
                for prefix in aff.PFX.get(flag, [])
                if prefix.crossproduct and not prefix in prefixes and prefix.cond_regexp.search(suffixed)
            ]
            for prefix in prefixes + additional_prefixes:
                root = suffixed[0:-len(prefix.strip)] if prefix.strip else suffixed
                prefixed = prefix.add + root
                result.add(prefixed)

                secondary_prefixes = [
                    prefix2
                    for flag in prefix.flags
                    for prefix2 in aff.PFX.get(flag, [])
                    if prefix2.crossproduct and prefix2.cond_regexp.search(prefixed)
                ]
                for prefix2 in secondary_prefixes:
                    root = prefixed[0:-len(prefix2.strip)] if prefix2.strip else prefixed
                    result.add(prefix2.add + root)

        secondary_suffixes = [
            suffix2
            for flag in suffix.flags
            for suffix2 in aff.SFX.get(flag, [])
            if suffix2.cond_regexp.search(suffixed)
        ]
        for suffix2 in secondary_suffixes:
            root = suffixed[0:-len(suffix2.strip)] if suffix2.strip else suffixed
            result.add(root + suffix2.add)

    for prefix in prefixes:
        root = word.stem[len(prefix.strip):]
        prefixed = prefix.add + root
        if not (aff.NEEDAFFIX and aff.NEEDAFFIX in prefix.flags):
            result.add(prefixed)

        if prefix.crossproduct:
            additional_suffixes = [
                suffix
                for flag in prefix.flags
                for suffix in aff.SFX.get(flag, [])
                if suffix.crossproduct and not suffix in suffixes and suffix.cond_regexp.search(prefixed)
            ]
            for suffix in suffixes + additional_suffixes:
                root = prefixed[0:-len(suffix.strip)] if suffix.strip else prefixed
                suffixed = root + suffix.add
                result.add(suffixed)

                secondary_suffixes = [
                    suffix2
                    for flag in suffix.flags
                    for suffix2 in aff.SFX.get(flag, [])
                    if suffix2.crossproduct and suffix2.cond_regexp.search(suffixed)
                ]
                for suffix2 in secondary_suffixes:
                    root = suffixed[0:-len(suffix2.strip)] if suffix2.strip else suffixed
                    result.add(root + suffix2.add)

    return result
